import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect
} from "react-router-dom";

import Login from './Components/Login';
import Error from './Components/Error';
import Flights from './Components/Flights';
import AddFlight from './Components/AddFlight';
import Navbar from './Components/Navbar';

import 'normalize.css'
import 'bootstrap/dist/css/bootstrap.css';
import './App.scss';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      flights: [
        {
          id: 1,
          from: 'tlv',
          to: 'lax',
          departureTime: '19:24',
          landingTime: '12:25',
          price: '120'
        },
        {
          id: 2,
          from: 'tlv',
          to: 'lax',
          departureTime: '19:24',
          landingTime: '12:25',
          price: '120'
        },
      ],
      isAuthenticated: true
    }
  }

  addFlight = (newFlight) => {
    this.setState({
      flights: [...this.state.flights, newFlight]
    })
  };

  login = (credentials) => {
    if (credentials.password.length === 0) return;

    this.setState({ isAuthenticated: true });
  };

  logout = () => {
    this.setState({ isAuthenticated: false });
  };

  render() {
    const isAuthenticated = this.state.isAuthenticated;
    const PrivateRoute    = ({ component: Component, ...rest }) => (
      <Route
        {...rest}
        render={props =>
          this.state.isAuthenticated ? (
            <Component {...props} {...rest} />
          ) : (
            <Redirect
              to={{
                pathname: "/error",
              }}
            />
          )
        }
      />
    );

    return (
      <Router>
        <div className="App">
          <Navbar isAuthenticated={isAuthenticated}
                  onLogout={this.logout}/>
          <header className="App-header">

            <>
              <Route exact path="/"
                     render={() => {
                       return isAuthenticated
                         ? <Redirect to="/flights"></Redirect>
                         : <Redirect to="/login"></Redirect>
                     }}/>

              <Route exact path="/login" render={() => {
                return isAuthenticated
                  ? <Redirect to="/flights"></Redirect>
                  : <Login onLogin={this.login}/>

              }}/>

              <PrivateRoute exact path="/flights"
                            flights={this.state.flights}
                            component={Flights}/>

              <Route exact path="/error" component={Error}/>

              <PrivateRoute exact path="/add-flight"
                            component={AddFlight}
                            onAdd={this.addFlight}/>
            </>

          </header>
        </div>
      </Router>
    );
  }
}

export default App;
