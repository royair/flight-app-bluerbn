import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './Login.scss';

class Error extends Component {
  constructor(props) {
    super(props);

  }


  render() {
    return (
      <>
        <p>access denied!</p>
        <Link to={'/login'}>log in</Link>
        </>
    )
  }
}

export default Error;