import React, { Component } from 'react';
import uuiv1d from 'uuid';
import './Login.scss';

class AddFlight extends Component {
  constructor(props) {
    super(props);

    this.state = {
      newFlight: {
        id: uuiv1d(),
        from: '',
        to: '',
        departureTime: '',
        landingTime: '',
        price: ''
      }
    }
  }

  onChange = (e) => {
    const newFlight = {
      ...this.state.newFlight,
      [e.target.name]: e.target.value
    };

    this.setState({ newFlight });
  };

  onSubmit = (e) => {
    e.preventDefault();
    e.stopPropagation();

    this.props.onAdd(this.state.newFlight);
  };

  render() {
    return (
      <>
        <h1>Add Flight</h1>
        <form className={'form-login'}
              onSubmit={this.onSubmit}>
          <div className="form-group">
            <label htmlFor="">From:</label>
            <input type="text"
                   className="form-control"
                   name="from"
                   onChange={this.onChange}
                   aria-describedby="emailHelp"/>
          </div>
          <div className="form-group">
            <label htmlFor="">To:</label>
            <input type="text"
                   name="to"
                   onChange={this.onChange}
                   className="form-control"/>
          </div>
          <div className="form-group">
            <label htmlFor="">Departure Time:</label>
            <input type="text"
                   name="departureTime"
                   onChange={this.onChange}
                   className="form-control"/>
          </div>
          <div className="form-group">
            <label htmlFor="">Landing Time</label>
            <input type="text"
                   name="landingTime"
                   onChange={this.onChange}
                   className="form-control"/>
          </div>
          <div className="form-group">
            <label htmlFor="">Price</label>
            <input type="text"
                   name="price"
                   onChange={this.onChange}
                   className="form-control"/>
          </div>
          <button type="submit" className="btn btn-primary">Submit
          </button>
        </form>
      </>
    )
  }
}

export default AddFlight;
