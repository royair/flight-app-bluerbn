import React, { Component } from 'react';
import './Login.scss';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      credentials: {
        username: '',
        password: ''
      }
    }
  }

  onChangeUsername = (e) => {
    const credentials    = Object.assign({}, this.state.credentials);
    credentials.username = e.target.value;

    this.setState({ credentials });
  };

  onChangePassword = (e) => {
    const credentials    = Object.assign({}, this.state.credentials);
    credentials.password = e.target.value;

    this.setState({ credentials });
  };

  onSubmit = (e) => {
    e.preventDefault();
    e.stopPropagation();

    this.props.onLogin(this.state.credentials);
  };

  render() {
    return (
      <>
        <h1>login</h1>

        <form className="form-login" onSubmit={this.onSubmit}>
          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Email address</label>
            <input type="email" className="form-control"
                   id="exampleInputEmail1" aria-describedby="emailHelp"
                   placeholder="Enter email"
                   onChange={this.onChangeUsername}/>
            <small id="emailHelp" className="form-text text-muted">We'll
              never share your email with anyone else.
            </small>
          </div>
          <div className="form-group">
            <label htmlFor="exampleInputPassword1">Password</label>
            <input type="password"
                   className="form-control"
                   id="exampleInputPassword1"
                   placeholder="Password"
                   onChange={this.onChangePassword}/>
          </div>

          <button type="submit" className="btn btn-primary">Submit</button>
        </form>
      </>
    )
  }
}

export default Login
