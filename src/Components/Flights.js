import React, { Component } from 'react';
import './Login.scss';

class Flights extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchText: ''
    }
  }

  getFlights = () => {
    const { searchText } = this.state;
    const flights        = this.props.flights;

    if (this.state.searchText.length > 0) {
      return flights.filter(flight => flight.to.includes(searchText));
    }

    return this.props.flights;
  };

  onChangeSearchText = (e) => {
    this.setState({ searchText: e.target.value });
  };

  render() {
    const flights = this.getFlights().map((flight, index) => (
      <tr key={flight.id}>
        <th scope="row">{index + 1}</th>
        <td>{flight.from}</td>
        <td>{flight.to}</td>
        <td>{flight.departureTime}</td>
        <td>{flight.landingTime}</td>
        <td>{flight.price}</td>
      </tr>
    ));

    return (
      <>
        <div className="flights">

          <form>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Search for
                Destination:</label>
              <input type="destination" className="form-control"
                     id="exampleInputDestination"
                     aria-describedby="destinationHelp"
                     placeholder="Enter destination"
                     onChange={this.onChangeSearchText}/>
            </div>
          </form>


          <table className="table table-striped">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">From</th>
                <th scope="col">To</th>
                <th scope="col">Departure Time</th>
                <th scope="col">Landing Time</th>
                <th scope="col">Price</th>
              </tr>
            </thead>
            <tbody>
              {flights}
            </tbody>
          </table>

        </div>
      </>
    )
  }
}

export default Flights;
