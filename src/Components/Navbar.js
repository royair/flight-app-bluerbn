import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';


class Navbar extends Component {
  constructor(props) {
    super(props);

  }

  logout = (history) => {
    this.props.onLogout();
    history.push('/');
  };

  render() {
    const { isAuthenticated } = this.props;
    const AuthenticatedButton = withRouter(
      ({ history }) => <button
        className="btn btn-outline-success my-2 my-sm-0"
        type="submit"
        onClick={() => this.logout(history)}>logout</button>
    );

    return (
      <>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <a className="navbar-brand" href="#">Flight App</a>
          <button className="navbar-toggler"
                  type="button"
                  data-toggle="collapse"
                  data-target="#navbarSupportedContent"
                  aria-controls="navbarSupportedContent" aria-expanded="false"
                  aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse"
               id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link className={'nav-link'} to="/">Home</Link>
              </li>
              <li className="nav-item">
                <Link className={'nav-link'} to="/flights">Flights</Link>
              </li>
              <li className="nav-item">
                <Link className={'nav-link'} to="/add-flight">Add Flight</Link>
              </li>
            </ul>
            <form className="form-inline my-2 my-lg-0">
              {isAuthenticated
                ? <AuthenticatedButton/>
                : <button className="btn btn-outline-success my-2 my-sm-0"
                          type="submit">login
                </button>
              }
            </form>
          </div>
        </nav>
      </>
    )
  }
};

export default Navbar;